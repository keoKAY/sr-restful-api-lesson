package com.kshrd.srrestfulapidemo.controller.restcontroller;

import com.kshrd.srrestfulapidemo.dto.request.UserLoginRequest;
import com.kshrd.srrestfulapidemo.dto.response.UserLoginResponse;
import com.kshrd.srrestfulapidemo.model.AuthUser;
import com.kshrd.srrestfulapidemo.model.UserResponse;
import com.kshrd.srrestfulapidemo.security.UserDetailImp;
import com.kshrd.srrestfulapidemo.security.jwt.JwtUtils;
import com.kshrd.srrestfulapidemo.service.StudentService;
import com.kshrd.srrestfulapidemo.utils.response.Response;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AuthRestController {



    @Autowired
    StudentService studentService;

    @Autowired
    AuthenticationManager authenticationManager;


    ModelMapper modelMapper = new ModelMapper();
    @PostMapping("/login")
    Response<UserLoginResponse> login (@RequestBody UserLoginRequest request){

        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(
                        request.getUsername(), request.getPassword()
                );
        try{
            Authentication authentication = authenticationManager.authenticate(authenticationToken);

            JwtUtils jwtUtils  = new JwtUtils();
            // jwtToken
            String token = jwtUtils.generateJwtToken(authentication);

            UserLoginResponse response = new UserLoginResponse();

            try{

                UserResponse findUserByUsername = studentService.findUserByUsername(request.getUsername());
                response =modelMapper.map(findUserByUsername,UserLoginResponse.class);
               //response.setUsername(findUserByUsername.getUsername());


            }catch (Exception exception){
                System.out.println("Error when finding the user by username");
                return Response.<UserLoginResponse>exception().setError("Failed to find the user by the provided username !");

            }

          response.setToken(token);

            return Response.<UserLoginResponse>ok().setPayload(response).setError("Login successfullly");
        }catch (Exception exception){
            System.out.println("Exception occur when trying to login for this credential : "+ exception.getMessage());

       return Response.<UserLoginResponse>exception().setError("Failed to perform the authentication due the the exception issues !");

        }

    }


    @GetMapping("/check")
    public String checkLoginUser(@AuthenticationPrincipal UserDetailImp login){

        try{

            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>..");
            System.out.println("Here is the credential of the login user ");
            System.out.println(login);

             if (login.getAuthorities().contains(" ADMIN")){
//.....
                 System.out.println("88888888888888888888888");
                 System.out.println("So you are the admin ");
             }  else{
                 System.out.println(" You are not the admin");
             }

            // if ( login.get == "ADMIN")

            return "Someone is login ..";
        }catch (Exception ex){
            System.out.println("There is no login user ");
            return "No one is login : ";
        }

    }


}
