package com.kshrd.srrestfulapidemo.controller.restcontroller;

import com.kshrd.srrestfulapidemo.dto.RegisterDto;
import com.kshrd.srrestfulapidemo.mapper.StudentMapper;
import com.kshrd.srrestfulapidemo.model.Student;
import com.kshrd.srrestfulapidemo.service.StudentService;
import com.kshrd.srrestfulapidemo.utils.paging.Paging;
import com.kshrd.srrestfulapidemo.utils.response.Response;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/api/v1/student")
public class StudentRestController {

    // inject
    @Autowired
    private StudentService studentService;
    @Autowired
    private StudentMapper studentMapper;

    ModelMapper modelMapper= new ModelMapper();
    // GetMapping, Put ...
    Student student = null;
    @GetMapping("/get-all-student")
    public List<Student> getAllStudent(){

//        try{
////            student.getUsername();
//            System.out.println(1/0); // arithmetic
//        }catch (Exception ex){
//            System.out.println(ex.getMessage());
//        }

        System.out.println("Here is the student information : ");
        studentService.getAllStudent().stream().forEach(System.out::println);
        return studentService.getAllStudent();
    }

    @GetMapping
    public ResponseEntity<?> getAllStudents(){

        HashMap<String,Object> response = new HashMap<String,Object>();
        try{
        List<Student> students= studentService.getAllStudent();

        response.put("error","null");
        response.put("message","retrieve the students successfully!");
        response.put("status", HttpStatus.OK);
        response.put("payload",students);

        return ResponseEntity.ok().body(response);
        }
        catch (Exception ex){
            System.out.println("Error on fetching the student data :"+ex.getMessage());
            response.put("error",ex.getMessage());
            response.put("message","failed to retrieve the student.");
            response.put("status", HttpStatus.NOT_FOUND);
            response.put("payload",null);

            return ResponseEntity.ok().body(response);
        }

    }

    @PostMapping("/register")
    public ResponseEntity<RegisterDto> registerStudent(@RequestBody Student student){

        RegisterDto response = new RegisterDto();

        // insert
        try{

            System.out.println("Request Student:"+ student);
            int insertID = studentService.registerStudent(student);
            System.out.println("insert id : "+insertID);

           Student findStudentByID = studentService.getStudentByID(insertID);

          /* response.setGender(findStudentByID.getGender());
           response.setUsername(findStudentByID.getUsername());
*/
            // using mapstruct
           // response = studentMapper.studentModelToDto(student);
//     student = studentMapper.dtoToStudentModel(response);


            // using modelmapper
      response = modelMapper.map(student,RegisterDto.class);

        }catch (Exception exception){

            System.out.println("insert exception sql : "+exception.getMessage());
        }

        // return something back
     return   ResponseEntity.ok().body(response);


    }



//     start from here
    @GetMapping("/findAllStudents")
    public Response<List<Student>> findAllStudents(@RequestParam(defaultValue ="10") int limit,@RequestParam(defaultValue = "1")  int page ,@RequestParam(defaultValue = "") String filter){
       List<Student> findAllStudents = new ArrayList<>();




        try {

            Paging paging =  new Paging();
            paging.setPage(page);
            int offset = (page -1)* limit;
            paging.setLimit(limit);

            // totalCount
//            findAllStudents = studentService.getAllStudent();
            findAllStudents = studentService.findStudentByPagination(limit,offset,filter);
            return Response.<List<Student>>ok().setPayload(findAllStudents).setError("Students fetched successfully!").setMetadata(paging);

        }catch (Exception ex){
            System.out.println(" Fetching data exception: "+ex.getMessage());
            return Response.<List<Student>>exception().setError("Failed to retrieve the student due the sql exception...");
        }

    }


    @PostMapping("/registerNewStudent")
    public Response<RegisterDto> registerNewUser(@RequestBody Student student){
        try{
            // insert
            int newInsertedId = studentService.registerStudent(student);
            Student newStudent = studentService.getStudentByID(newInsertedId);

            // map
            RegisterDto response = modelMapper.map(newStudent,RegisterDto.class);

            return Response.<RegisterDto>successCreate().setPayload(response).setError("Created Successfully!").setSuccess(true);


        }catch (Exception ex){
            System.out.println("Insert Student Exception ; "+ex.getMessage());

            return Response.<RegisterDto>exception().setError("Failed to register student due to the sql exception");

        }

    }



}
