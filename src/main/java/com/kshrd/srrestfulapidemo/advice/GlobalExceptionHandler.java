package com.kshrd.srrestfulapidemo.advice;

import com.kshrd.srrestfulapidemo.dto.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Deprecated
public class GlobalExceptionHandler {

/// create any exception handler method
    @ExceptionHandler({NullPointerException.class})
    public ResponseEntity<?> nullExceptionHandler(){

        ApiError error = new ApiError();
        error.setMessage("Error on null pointer ");
        error.setStatus(HttpStatus.BAD_REQUEST);

        return ResponseEntity.ok().body(error);

    }

    @ExceptionHandler({ArithmeticException.class})
    public ResponseEntity<?> arithmeticExceptionHandler(){

        ApiError error = new ApiError();
        error.setMessage("Error your math is sucks! ");
        error.setStatus(HttpStatus.BAD_REQUEST);

        return ResponseEntity.ok().body(error);

    }







}
