package com.kshrd.srrestfulapidemo.service;

import com.kshrd.srrestfulapidemo.model.AuthUser;
import com.kshrd.srrestfulapidemo.model.Student;
import com.kshrd.srrestfulapidemo.model.UserResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface StudentService {
    UserResponse findUserByUsername(String username);

    List<Student> getAllStudent();

    int registerStudent(Student student);

    Student getStudentByID(int id);
    List<Student> findStudentByPagination(int limit, int offset,String filter);


}
