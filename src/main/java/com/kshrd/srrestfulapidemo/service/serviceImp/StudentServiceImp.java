package com.kshrd.srrestfulapidemo.service.serviceImp;

import com.kshrd.srrestfulapidemo.model.AuthUser;
import com.kshrd.srrestfulapidemo.model.Student;
import com.kshrd.srrestfulapidemo.model.UserResponse;
import com.kshrd.srrestfulapidemo.repository.StudentRepository;
import com.kshrd.srrestfulapidemo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImp implements StudentService {


    // inject
    @Autowired
    private StudentRepository studentRepository;

    @Override
    public UserResponse findUserByUsername(String username) {
        return studentRepository.findByUsername(username);
    }

    @Override
    public List<Student> getAllStudent() {
        return studentRepository.findAllStudent();
    }

    @Override
    public int registerStudent(Student student) {
        return studentRepository.register(student);
    }

    @Override
    public Student getStudentByID(int id) {
        return studentRepository.findByID(id);
    }

    @Override
    public List<Student> findStudentByPagination(int limit, int offset, String filter) {
        return studentRepository.findStudentByPagination(limit,offset,filter);
    }
}
