package com.kshrd.srrestfulapidemo.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    @Bean
    public Docket api(){
        return  new Docket(DocumentationType.SWAGGER_2)
                .enable(true)
                .apiInfo(apiInfo())
                .ignoredParameterTypes(AuthenticationPrincipal.class)
                .securityContexts(Collections.singletonList(securityContext()))
                .securitySchemes(Collections.singletonList(apiKey()))
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.kshrd.srrestfulapidemo.controller.restcontroller"))
                .paths(PathSelectors.any())
                .build();
    }

    // security schema

    private ApiKey apiKey(){

       return new ApiKey("JWT","Authorization","header");}




    //  // security context
    private SecurityContext securityContext(){
        return  SecurityContext.builder().securityReferences(defaultAuth()).build();
    }

    private List<SecurityReference> defaultAuth(){
        AuthorizationScope authorizationScope = new AuthorizationScope("global","accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;

        return Collections.singletonList(new SecurityReference("JWT",authorizationScopes));
    }




 // (String title, String description, String version, String termsOfServiceUrl, Contact contact,
 // String license, String licenseUrl, Collection<VendorExtension> vendorExtensions)
    public ApiInfo apiInfo(){
        return  new ApiInfo(
                "SR API Restful Web Service ",
                "This is the description of the API",
                "1.0",
                "termOfService",
               new Contact("sr","url","sr@gmail.com"),
                "license"
                ,"license url ",
                Collections.emptyList()

        );

    }
}
