package com.kshrd.srrestfulapidemo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor

public class UserResponse {

    private int id;
    private String username;
    private  String gender;
    private String bio;
    private List<String> roles;


}
