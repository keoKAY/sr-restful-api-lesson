package com.kshrd.srrestfulapidemo.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Student {

    private int id;
    private String username;
    private  String gender;
    private String bio;
    private String password;
    private int course_id;
}
