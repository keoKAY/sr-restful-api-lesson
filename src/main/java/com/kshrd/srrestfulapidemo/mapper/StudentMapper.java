package com.kshrd.srrestfulapidemo.mapper;

import com.kshrd.srrestfulapidemo.dto.RegisterDto;
import com.kshrd.srrestfulapidemo.model.Student;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public interface StudentMapper {

    // map from reponse -> request
    Student dtoToStudentModel(RegisterDto registerDto);
    //map from request -> response
    RegisterDto studentModelToDto(Student student);
}
