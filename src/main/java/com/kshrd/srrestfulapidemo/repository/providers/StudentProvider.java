package com.kshrd.srrestfulapidemo.repository.providers;

import org.apache.ibatis.jdbc.SQL;

public class StudentProvider {


 public String getAllStudent(int limit, int offset,String filter){

        return new SQL(){{

            System.out.println("Filter value : "+filter);

            if (!filter.isEmpty()){
                System.out.println(" Filter is not empty...");
                SELECT("*");
                FROM("students");
                LIMIT("#{limit}");
                OFFSET("#{offset}");
                WHERE("UPPER (bio) like upper('%'||#{filter}||'%')");
            }else {
                SELECT("*");
                FROM("students");
                LIMIT("#{limit}");
                OFFSET("#{offset}");

            }






        }}.toString();
    }


}
