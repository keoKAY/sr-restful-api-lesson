package com.kshrd.srrestfulapidemo.repository;

import com.kshrd.srrestfulapidemo.model.AuthUser;
import com.kshrd.srrestfulapidemo.model.Student;
import com.kshrd.srrestfulapidemo.model.UserResponse;
import com.kshrd.srrestfulapidemo.repository.providers.StudentProvider;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface StudentRepository {

// For the authentication purposes

    @Select("Select * from students where username =#{username}")
    @Results({
            @Result(property = "roles", column = "id" , many = @Many(select = "FindRolesById"))
    })
   AuthUser findUserByUsername(String username);

    @Select("select role_name from roles inner join student_role sr on roles.id = sr.role_id where user_id =#{id}")
    List<String> FindRolesById(int id );

    @Select("Select * from students where username =#{username}")
    @Results({
            @Result(property = "roles", column = "id" , many = @Many(select = "FindRolesById"))
    })
    UserResponse findByUsername(String username);





    @SelectProvider(type = StudentProvider.class, method ="getAllStudent" )
    List<Student> findStudentByPagination(int limit, int offset,String filter);


    @Select("select * from students")
    List<Student> findAllStudent();



    @Select("insert into students(id,username,gender,bio, password,course_id) " +
            "values (#{student.id},#{student.username}," +
            "#{student.gender},#{student.bio},#{student.password}, #{student.course_id}) returning id ")
    int register(@Param("student") Student student );

    @Select("select * from students where id=#{id}")
    Student findByID(int id);





}
