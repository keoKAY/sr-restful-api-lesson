package com.kshrd.srrestfulapidemo.dto.request;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class UserLoginRequest {
    private String username;
    private String password;
}
