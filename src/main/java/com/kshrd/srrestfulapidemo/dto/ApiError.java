package com.kshrd.srrestfulapidemo.dto;


import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class ApiError {

    private String message ;
    private HttpStatus status;
}
